<?php
/**
 * @file
 * Admin page callbacks for entity_override.
 */

/**
 * Page callback for devel/entity/info/override.
 */
function entity_override_page_devel() {

  $override_info = entity_override_get_info();
  // dsm() existence checked in entity_override_menu().
  ksort($override_info);
  $ret = kprint_r($override_info, TRUE, 'override_info');

  $entities = _entity_override_get_managed_entities();
  $ret .= kprint_r($entities, TRUE, 'managed entities');

  module_load_install('entity_override');
  $schema = entity_override_schema();
  $ret .= kprint_r($schema, TRUE, 'schema');

  $q = db_select('entity_override_config', 'eoc')
    ->fields('eoc', array('module', 'table_name'))
    ->orderBy('module')
    ->orderBy('table_name');
  $result = $q->execute();
  $rows = array();
  foreach ($result as $row) {
    $rows[] = array('data' => $row);
  }

  if (empty($rows)) {
    $ret .= t('No entity tables');
  }
  else {
    $ret .= theme('table', array(
      'caption' => t('Entity tables'),
      'header' => array(t('Module'), t('Table')),
      'rows' => $rows,
    ));
  }

  return $ret;
}
