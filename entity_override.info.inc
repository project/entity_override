<?php

/**
 * Implements hook_entity_property_info_alter().
 *
 * Override the automatic metadata typing for created and changed: these
 * are timestamps, not just integers.
 */
function entity_override_entity_property_info_alter(&$info) {
  foreach (array_keys(_entity_override_get_managed_entities()) as $entity_type) {
    $created = &$info[$entity_type]['properties']['created'];
    $changed = &$info[$entity_type]['properties']['changed'];
    $properties['created'] = array(
      'label' => t("Date created"),
      'type' => 'date',
      'description' => t('The date the entity was created.'),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => "CRUD $entity_type entities",
      'schema field' => 'created',
    );
    $properties['changed'] = array(
      'label' => t("Date changed"),
      'type' => 'date',
      'description' => t('The date the entity was most recently updated.'),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => "CRUD $entity_type entities",
      'schema field' => 'changed',
    );

    foreach (array('created', 'changed') as $property_name) {
      foreach ($properties[$property_name] as $attribute_name => $attribute_value) {
        $info[$entity_type]['properties'][$property_name][$attribute_name] = $attribute_value;
      }
    }
  }
}
