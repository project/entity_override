<?php
/**
 * @file
 * Drush commands for Entity Override
 *
 * - eog: plot field overrides on a site
 */

class EntityOverrideGraph {
  var $einfo;
  var $fields;
  var $instances;
  var $g;
  var $debugLevel;

  public function debug($message) {
    if ($this->debugLevel) {
      fputs(STDERR, $message);
    }
  }

  public function __construct($debugLevel = 0) {
    $this->fields = field_info_fields();
    $this->instances = field_info_instances();
    $this->g = new Image_Graphviz();
    $this->g->setDirected(TRUE);
    $this->g->setAttributes(array(
      'rankdir' => 'LR',
      'overlap' => FALSE,
    ));
    $this->debugLevel = $debugLevel;
  }

  public function ensureCluster($name) {
    $groups = $this->g->_getGroups();
    $levels = explode(':', $name);
    $current = $levels[0];
    $this->debug("Ensuring $name");
    if (!in_array($current, $groups)) {
      $this->debug("Fixing $current");
      $this->g->addCluster($current, " $current ", array(
        'style' => 'filled, rounded, bold',
        'fillcolor' => '#f0f0f0',
        'fontsize' => 14.0,
      ));
    }
    array_shift($levels);
    foreach ($levels as $level) {
      if (!in_array("$current:$level", $groups)) {
        $this->debug("Fixing $current:$level");
        $this->g->addCluster("$current:$level", " $level ", array(
          'penwidth' => 0.5,
          'fillcolor' => '#ffffff',
          'fontsize' => 10.0,
        ), $current);
      }
      $current .= $level;
    }
  }

  public function render() {
    $g = &$this->g;
    foreach ($this->instances as $entity_type => $bundles) {
      foreach ($bundles as $bundle => $instances) {
        if (empty($bundle)) {
          $bundle = '<empty>';
        }
        foreach ($instances as $field_name => $instance) {
          $field = $this->fields[$field_name];
          if ($field['type'] != 'entity_override_field') {
            continue;
          }
          foreach ($field['settings']['fields'] as $overridden_field_name) {
            $this->debug(sprintf("Adding %-12s %s", 'field', $overridden_field_name));
            $target_type = $field['settings']['target_type'];
            $target_bundle = $field['settings']['target_bundle'];
            if (empty($target_bundle)) {
              $target_bundle = '<any>';
            }
            $this->ensureCluster($entity_type);
            $this->ensureCluster("$entity_type:$bundle");
            $this->ensureCluster($target_type);
            $this->ensureCluster("$target_type:$target_bundle");

            $direction = $field['settings']['override_direction'];
            $g->addNode("$entity_type:$bundle:$overridden_field_name", array(
              'label' => $overridden_field_name,
              'shape' => 'none',
              'fontsize' => 12.0,
            ), "$entity_type:$bundle");
            $g->addNode("$target_type:$target_bundle:$overridden_field_name", array(
              'label' => $overridden_field_name,
              'shape' => 'none',
              'fontsize' => 12.0,
            ), "$target_type:$target_bundle");
            $this->debug(sprintf("Adding %-14s %s", 'edge', "$entity_type:$bundle:$overridden_field_name -> $target_type:$target_bundle:$overridden_field_name"));
            $edge = array("$entity_type:$bundle:$overridden_field_name" => "$target_type:$target_bundle:$overridden_field_name");
            $g->addEdge($direction == 'to' ? $edge : array_flip($edge), array(
              'color' => $direction == 'to' ? 'red' : 'green',
              'style' => $direction == 'to' ? 'solid' : 'dashed',
              'label' => $field_name,
              'fontsize' => 10.0,
            ));
          }
        }
      }
    }

    // print_r($g->_getGroups());
    $g->image('svg', 'dot');
    // echo $g->parse();

  }
}

function entity_override_drush_command() {
  $items = array();

  $items['entity-override-graph'] = array(
    'description' => 'Plot field overrides',
    'arguments' => array(),
    'options' => array(),
    'examples' => array(),
    'aliases' => array('eog'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL, // full entity/field system needed
  );

  return $items;
}


function drush_entity_override_graph_validate() {
  if (!@include_once 'Image/GraphViz.php') {
    drush_set_error('PEAR Image_Graphviz not found.');
  }

  /*
  // Native
  $ret = _entity_override_check_graphviz($info);
  if (!$ret) {
    drush_set_error('Graphviz not found.', implode(' ', $info));
  }
  */
  return $ret;
}

/**
 * Run DOT to obtain its version information.
 *
 * @param array $matches
 *   - full output
 *   - command name, should be "dot"
 *   - version string
 *   - build string
 *
 * @return boolean
 */
function _entity_override_check_graphviz(&$matches = NULL) {

  $command_args = '-V';
  $matches = array();

  if (eog_rundot('-V', NULL, $stdout, $stderr)) {
    $regex = '/^(\w+) - graphviz version (\d+\.\d+.\d+) \((.*)\)$/';
    $ret = preg_match($regex, $stderr, $matches);
    if ($ret != 1) {
      $matches = array();
      $ret = FALSE;
    }
    else {
      // Use version info from $matches if needed.
      $ret = TRUE;
    }
  }
  else {
    $ret = FALSE;
  }

  return $ret;
}

/**
 * Command callback for eog.
 */
function drush_entity_override_graph() {
  // $iinfo = field_info_instances();
  $graph = new EntityOverrideGraph();
  echo $graph->render();
}

/**
 * Perform a call to DOT, using the three command channels as strings.
 *
 * @param string $args
 * @param string $stdin
 * @param string $stdout
 * @param string $stderr
 *
 * @return boolean
 *   FALSE on error, TRUE on success.
 */
function eog_rundot($args, $stdin, &$stdout, &$stderr) {
  $dot_path = 'dot';
  $pipes = NULL;
  $drupal_path = getcwd();

  $descriptors = array(
    0 => array('pipe', 'r'), // stdin
    1 => array('pipe', 'w'), // stdout
    2 => array('pipe', 'w')  // stderr
  );

  if (is_resource($h = proc_open($dot_path .' '. $args, $descriptors, $pipes, $drupal_path))) {
    $stdout = $stderr = '';
    if (!empty($stdin)) {
      fwrite($pipes[0], $stdin);
    }

    while (!feof($pipes[1])) {
      $stdout .= fgets($pipes[1]);
    }

    while (!feof($pipes[2])) {
      $stderr .= fgets($pipes[2]);
    }


    fclose($pipes[0]);
    fclose($pipes[1]);
    fclose($pipes[2]);
    $ret = proc_close($h);
    $ret = ($ret === 0);
  }
  else {
    $ret = FALSE;
  }

  return $ret;
}
