<?php
function entity_override_test_entity_override_info() {
  $override_name = 'override_story';
  $entity_name = "${override_name}_store";

  $ret = array(
    $override_name => array(
      // 'override_type' default: 'push'
      'destination_type' => 'node',
      'destination_bundle' => 'article', // could be defaulted
      'source_management' => TRUE, // Ask EO to manage the source entity/bundle
      'source_type' => $entity_name, // Push fields are instanciated on source
      'source_bundle' => $entity_name, // could be defaulted
      'fields' => array('field_tags'), // 'body' is locked to node entities
      'key_callback' => array('EntityOverrideContext', 'get'),
      'key_allowed_values_callback' => array('EntityOverrideContext', 'getAllowed'),
    ),

//    'override_master_product_pull2' => array(
//      'override_type' => 'pull', // Default: 'push'
//      'destination_type' => 'commerce_product', // Pull fields are instanciated on destination
//      'destination_bundle' => 'product', // could be defaulted
//      // Existing entity: will not be touched by EO
//      'source_type' => 'cartier_master_product',
//      'source_bundle' => 'cartier_master_product', // could be defaulted
//      'fields' => array('c_product_name'),
//      // No callbacks on pull fields
//    ),
  );
  return $ret;
}
