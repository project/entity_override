<?php

/**
 * @file
 * Support for processing entity override fields in Migrate.
 */

/**
 * Implement hook_migrate_api().
 */
function entity_override_migrate_api() {
  $api = array(
    'api' => 2,
  );
  return $api;
}

class MigrateEntityOverrideFieldHandler extends MigrateFieldHandler {
  public function __construct() {
    $this->registerTypes(array('entity_override_field'));
  }

  public function prepare($entity, array $field_info, array $instance, array $values) {
    if (isset($values['arguments'])) {
      $arguments = $values['arguments'];
      unset($values['arguments']);
    }
    else {
      $arguments = array();
    }

    // Setup the standard Field API array for saving.
    $delta = 0;
    foreach ($values as $value) {
      $return[LANGUAGE_NONE][$delta]['target_type'] = $field_info['settings']['target_type'];
      $return[LANGUAGE_NONE][$delta]['target_id'] = $value;
      $return[LANGUAGE_NONE][$delta]['key'] = isset($arguments['key']) ? $arguments['key'] : '';
      $delta++;
    }
    if (!isset($return)) {
      $return = NULL;
    }
    return $return;
  }
}
