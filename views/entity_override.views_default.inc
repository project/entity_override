<?php
/**
 * Default Views for Entity Override.
 */

/**
 * Implements hook_views_default_views().
 *
 * Discover all views/*.view.inc files.
 */
function entity_override_views_default_views() {
  $ret = array();
  $directory = new DirectoryIterator(dirname(__FILE__));
  foreach ($directory as $item) {
    if (!preg_match('/\.view\.inc$/', $item->getFilename())) {
      continue;
    }
    $view = NULL;
    include_once $item->getPathname();
    if (!is_object($view) || empty($view->name) || empty($view->tag) || $view->tag != 'Entity Override') {
      drupal_set_message(t('Incorrect view format in %file', array(
        '%file' => $item->getFilename(),
      )), 'error');
      watchdog('entity_override', 'Incorrect view format in %file', array(
        '%file' => $item->getFilename(),
      ), WATCHDOG_ERROR);
      continue;
    }
    $ret[$view->name] = $view;
  }

  $ret = array_merge($ret, _entity_override_entity_type_get_views() ?: array());
  return $ret;
}

/**
 * Build a simple list view for each managed entity.
 */
function _entity_override_entity_type_get_views() {
  $entities = _entity_override_get_managed_entities();
  $entity_info = entity_get_info();
  $views = array();

  foreach ($entities as $entity_type => $override_info) {
    $field = $override_info['#field'];
    $field_name = key($field);
    $field_override_info = $field[$field_name];
    $field_info = field_info_field($field_name);
    $field_table = _field_sql_storage_tablename($field_info);
    $title = _entity_override_machine_to_human($entity_type);
    $base_table = $entity_info[$field_override_info['source_type']]['base table'];

    $view = new view;
    $view->name = $entity_type;
    $view->description = strtr('A list of @type entities.', array('@type' => $entity_type));
    $view->tag = 'Entity Override';
    $view->base_table = $base_table;
    $view->human_name = $title;
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = $title;
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['access']['perm'] = 'access entity_override information';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['query']['options']['query_comment'] = FALSE;
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '10';
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array(
      'id' => 'id',
      'changed' => 'changed',
      'created' => 'created',
      $field_name => $field_name,
    );
    $handler->display->display_options['style_options']['default'] = 'id';
    $handler->display->display_options['style_options']['info'] = array(
      'id' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'changed' => array(
        'sortable' => 1,
        'default_sort_order' => 'desc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'created' => array(
        'sortable' => 1,
        'default_sort_order' => 'desc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      $field_name => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
    );
    $handler->display->display_options['style_options']['override'] = 1;
    $handler->display->display_options['style_options']['sticky'] = 0;
    $handler->display->display_options['style_options']['empty_table'] = 0;
    /* Field: entity: entity ID */
    $handler->display->display_options['fields']['id']['id'] = 'id';
    $handler->display->display_options['fields']['id']['table'] = $base_table;
    $handler->display->display_options['fields']['id']['field'] = 'id';
    /* Field: entity: Date changed */
    $handler->display->display_options['fields']['changed']['id'] = 'changed';
    $handler->display->display_options['fields']['changed']['table'] = $base_table;
    $handler->display->display_options['fields']['changed']['field'] = 'changed';
    /* Field: entity: Date created */
    $handler->display->display_options['fields']['created']['id'] = 'created';
    $handler->display->display_options['fields']['created']['table'] = $base_table;
    $handler->display->display_options['fields']['created']['field'] = 'created';
    /* Field: entity: field name*/
    $handler->display->display_options['fields'][$field_name]['id'] = $field_name;
    $handler->display->display_options['fields'][$field_name]['table'] = $field_table;
    $handler->display->display_options['fields'][$field_name]['field'] = $field_name;
    $handler->display->display_options['fields'][$field_name]['label'] = 'Override key';
    $handler->display->display_options['fields'][$field_name]['alter']['alter_text'] = 0;
    $handler->display->display_options['fields'][$field_name]['alter']['make_link'] = 0;
    $handler->display->display_options['fields'][$field_name]['alter']['absolute'] = 0;
    $handler->display->display_options['fields'][$field_name]['alter']['external'] = 0;
    $handler->display->display_options['fields'][$field_name]['alter']['replace_spaces'] = 0;
    $handler->display->display_options['fields'][$field_name]['alter']['trim_whitespace'] = 0;
    $handler->display->display_options['fields'][$field_name]['alter']['nl2br'] = 0;
    $handler->display->display_options['fields'][$field_name]['alter']['word_boundary'] = 1;
    $handler->display->display_options['fields'][$field_name]['alter']['ellipsis'] = 1;
    $handler->display->display_options['fields'][$field_name]['alter']['more_link'] = 0;
    $handler->display->display_options['fields'][$field_name]['alter']['strip_tags'] = 0;
    $handler->display->display_options['fields'][$field_name]['alter']['trim'] = 0;
    $handler->display->display_options['fields'][$field_name]['alter']['html'] = 0;
    $handler->display->display_options['fields'][$field_name]['element_label_colon'] = 1;
    $handler->display->display_options['fields'][$field_name]['element_default_classes'] = 1;
    $handler->display->display_options['fields'][$field_name]['hide_empty'] = 0;
    $handler->display->display_options['fields'][$field_name]['empty_zero'] = 0;
    $handler->display->display_options['fields'][$field_name]['hide_alter_empty'] = 1;
    $handler->display->display_options['fields'][$field_name]['click_sort_column'] = 'target_id';
    $handler->display->display_options['fields'][$field_name]['type'] = 'entity_override_key';
    $handler->display->display_options['fields'][$field_name]['field_api_classes'] = 0;

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page_1');
    $handler->display->display_options['path'] = "admin/content/entity-override/$entity_type";
    $handler->display->display_options['menu']['type'] = 'normal';
    $handler->display->display_options['menu']['title'] = $title;
    $handler->display->display_options['menu']['weight'] = '0';
    $handler->display->display_options['menu']['name'] = 'management';
    $handler->display->display_options['menu']['context'] = 0;
    $translatables[$view->name] = array(
      t('Master'),
      t('more'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Items per page'),
      t('- All -'),
      t('Offset'),
      t('.'),
      t(','),
      t('Date changed'),
      t('Date created'),
      t('Override key'),
      t('Page'),
    );

    $views[$view->name] = $view;
    unset($view);
  }
    return $views;
  }
