<?php
/**
 * @file
 * Views 3 integration
 *
 * - Views data: exposte
 *   - managed entities
 */

/**
 * Immplements hook_views_data().
 *
 * - expose EO managed entities as base tables
 */
function entity_override_views_data() {
  $ret = array();

  $ret['entity_override_config'] = array(
    'table' => array(
      'group' => t('Entity Override'),
      'base' => array(
        'field' => 'table_name',
        'title' => t('Entity Override managed table'),
        'help' => t('The table containing the managed entity ids and properties'),
      ),
    ),

    'table_name' => array(
      'title' => t('Table name'),
      'help' => t('The name of the table holding the managed entity data.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => 'views_handler_sort',
    ),

    'module' => array(
      'title' => t('Module'),
      'help' => t('The module in which the override was defined'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => 'views_handler_sort',
    ),
  );

  return $ret;
}

/**
 * Implements hook_views_data_alter().
 *
 * - Add inverse relationships between the field table and the overriding tables.
 */
function entity_override_views_data_alter(&$data) {
  foreach (field_info_fields() as $field_name => $field) {
    if ($field['type'] !== 'entity_override_field') {
      continue;
    }

    $entity_info = entity_get_info($field['settings']['target_type']);

    foreach ($field['bundles'] as $entity_type => $bundles) {
      $overriding_entity_info = entity_get_info($entity_type);

      $data[$entity_info['base table']][$field['field_name']]['relationship'] = array(
        'handler' => 'views_handler_relationship',

        'base' => $overriding_entity_info['base table'],
        'base field' => $overriding_entity_info['entity keys']['id'],
        'relationship table' => _field_sql_storage_tablename($field),
        'relationship field' => 'entity_id',

        'label' => t('!child overriding !parent from !field_name', array('!child' => $entity_type, '!parent' => $field['settings']['target_type'], '!field_name' => $field['field_name'])),
        'title' => t('!child overriding !parent from !field_name', array('!child' => $entity_type, '!parent' => $field['settings']['target_type'], '!field_name' => $field['field_name'])),
        'group' => t('Entity Override'),
        'help' => t('A !child overriding !parent from !field_name.', array('!child' => $entity_type, '!parent' => $field['settings']['target_type'], '!field_name' => $field['field_name'])),
      );
    }
  }
}
