<?php

class entity_override_handler_filter_key extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $field = field_info_field($this->definition['field_name']);
      $instance = field_info_instance($field['settings']['target_type'], $field['field_name'], $field['settings']['target_bundle']);
      if (!empty($field['settings']['key_allowed_values_callback'])) {
        $callback = $field['settings']['key_allowed_values_callback'];
        $this->value_options = $callback($field, $instance);
      }
      else {
        $this->value_options = array();
      }
    }
  }
}
