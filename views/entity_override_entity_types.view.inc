<?php
/**
 * @file
 * View showing managed entities.
 *
 * FIXME: technically, the rewriting links to the base table, but the
 *   entity list view is based on the entity type. This works because
 *   both have the same string value, but should be more robust.
 */

$view = new view;
$view->name = 'entity_override_entity_types';
$view->description = 'A list of entities automatically managed by Entity Override.';
$view->tag = 'Entity Override';
$view->base_table = 'entity_override_config';
$view->human_name = 'Entity Override managed entities';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Entity Override: managed entities';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access entity_override information';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'module' => 'module',
  'table_name' => 'table_name',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'module' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'table_name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['empty_table'] = 0;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'No EO manged entities';
$handler->display->display_options['empty']['area']['empty'] = FALSE;
$handler->display->display_options['empty']['area']['content'] = 'Entity Override is not currently managing any entity.';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
$handler->display->display_options['empty']['area']['tokenize'] = 0;
/* Field: Entity Override: Module */
$handler->display->display_options['fields']['module']['id'] = 'module';
$handler->display->display_options['fields']['module']['table'] = 'entity_override_config';
$handler->display->display_options['fields']['module']['field'] = 'module';
$handler->display->display_options['fields']['module']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['module']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['module']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['module']['alter']['external'] = 0;
$handler->display->display_options['fields']['module']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['module']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['module']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['module']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['module']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['module']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['module']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['module']['alter']['trim'] = 0;
$handler->display->display_options['fields']['module']['alter']['html'] = 0;
$handler->display->display_options['fields']['module']['element_label_colon'] = 1;
$handler->display->display_options['fields']['module']['element_default_classes'] = 1;
$handler->display->display_options['fields']['module']['hide_empty'] = 0;
$handler->display->display_options['fields']['module']['empty_zero'] = 0;
$handler->display->display_options['fields']['module']['hide_alter_empty'] = 1;
/* Field: Entity Override: Table name */
$handler->display->display_options['fields']['table_name']['id'] = 'table_name';
$handler->display->display_options['fields']['table_name']['table'] = 'entity_override_config';
$handler->display->display_options['fields']['table_name']['field'] = 'table_name';
$handler->display->display_options['fields']['table_name']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['table_name']['alter']['make_link'] = 1;
$handler->display->display_options['fields']['table_name']['alter']['path'] = 'admin/content/entity-override/[table_name]';
$handler->display->display_options['fields']['table_name']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['table_name']['alter']['external'] = 0;
$handler->display->display_options['fields']['table_name']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['table_name']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['table_name']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['table_name']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['table_name']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['table_name']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['table_name']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['table_name']['alter']['trim'] = 0;
$handler->display->display_options['fields']['table_name']['alter']['html'] = 0;
$handler->display->display_options['fields']['table_name']['element_label_colon'] = 1;
$handler->display->display_options['fields']['table_name']['element_default_classes'] = 1;
$handler->display->display_options['fields']['table_name']['hide_empty'] = 0;
$handler->display->display_options['fields']['table_name']['empty_zero'] = 0;
$handler->display->display_options['fields']['table_name']['hide_alter_empty'] = 1;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/content/entity-override';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Entity Override';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$translatables['entity_override_entity_types'] = array(
  t('Master'),
  t('Entity Override: managed entities'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('No EO manged entities'),
  t('Entity Override is not currently managing any entity.'),
  t('Module'),
  t('Table name'),
  t('admin/content/entity-override/[table_name]'),
  t('Page'),
);
