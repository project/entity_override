<?php

/**
 * Define the schema for the two tables of a managed entity.
 *
 * @see entity_override_schema
 *
 * @param string $entity_type
 *   The type of the entity.
 * @param unknown_type $info
 *   An entity info entry in entity_get_info() format.
 *
 * @return array
 *   A schema array for the base table for the entity_type.
 */
function _entity_override_get_entity_schema($entity_type, $info) {
  $base = $info['base table'];
  $ret[$base] = array(
    'description' => "The base table for $entity_type.",
    'fields' => array(
      'id' => array(
        'description' => "The primary identifier for a $entity_type.",
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => "The bundle of this $entity_type.",
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'created' => array(
        'description' => "The Unix timestamp when the $entity_type was created.",
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => "The Unix timestamp when the $entity_type was most recently saved.",
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      "${base}_changed" => array('changed'),
      "${base}_created" => array('created'),
      "${base}_type"    => array('type'),
    ),
    'primary key' => array('id'),
  );

  return $ret;
}

/**
 * Implements hook_field_schema().
 */
function entity_override_field_schema($field) {
  switch ($field['type']) {
    case 'entity_override_field':
      $columns = array(
        'target_id' => array(
          'type' => 'int',
          'description' => 'The id of the target entity',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'target_type' => array(
          'type' => 'varchar',
          'length' => 255,
          'description' => 'The type of the target entity',
          'not null' => TRUE,
        ),
        'key' => array(
          'type' => 'varchar',
          'length' => 4000,
          'description' => 'The key value of the override',
          'not null' => TRUE,
        ),
      );
      break;
  }

  return array(
    'columns' => $columns,
    'indexes' => array(
      'target_entity' => array('target_type', 'target_id'),
    ),
  );
}


/**
 * Implements hook_schema().
 */
function entity_override_schema() {
  $ret = array();

  $ret['entity_override_config'] = array(
    'description' => 'Stores information about the entites managed by entity_override.',
    'fields' => array(
      'module' => array(
        'description' => 'The module defining this entity.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'table_name' => array(
        'description' => 'A table created on behalf of this module',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('module', 'table_name'),
    'foreign keys' => array(
      'module' => array(
        'table' => 'system',
        'columns' => array('module' => 'name'),
      ),
    ),
  );

  // Do not use entity_get_info(), do not depend on cache to avoid a recursive
  // schema rebuild, use less memory, and only get the proper entities.
  module_load_include('module', 'entity_override');
  $entity_info = entity_override_entity_info();
  foreach ($entity_info as $entity_type => $info) {
    $ret = array_merge($ret, _entity_override_get_entity_schema($entity_type, $info));
  }
  return $ret;
}

/**
 * Placeholder to get a schema version.
 */
function entity_override_update_7000() {
}

